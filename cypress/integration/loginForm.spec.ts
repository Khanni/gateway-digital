import {userEmail, password} from "../../src/utils/loginConstants";
/*
  Integration test using Cypress to test the LoginForm component
 */
describe('<LoginForm/>', () => {
    beforeEach(() => {
        cy.visit('/');
    });
    it('user is able to login', () => {
        cy.findByLabelText('Email').as('emailInput').should('exist');
        cy.findByLabelText('Password').as('passwordInput').should('exist');
        cy.findByText('Login').should('exist')
        cy.get('@emailInput').clear().type(userEmail);
        cy.get('@passwordInput').clear().type(password);
        cy.findByText('Login').click();
        cy.findByTestId('loginForm').should('not.exist');
        cy.end();
    })
})