import React from "react";

function FooterComponent() {
    return (
        <div>
            © Gateway digital Ltd. All rights reserved.
        </div>
    )
}

export default FooterComponent