/*
Type to represent the racing result in the state
 */
import React from "react";

export interface RacingResult {
    id: number
    name: string,
    time: number
}

export interface RacingResultMap {
    [key: number]: RacingResult
}

export interface LoginRequestModel {
    email: string,
    password: string
}

export interface ActionTypes {
    type: 'ADD_TOKEN',
    payload: string
}

export interface ContextState {
    token: string
}

export interface ContextType {
    tokenState: ContextState,
    tokenDispatch: React.Dispatch<ActionTypes>
}