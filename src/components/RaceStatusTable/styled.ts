import styled from "styled-components";

export const StyledTh = styled.th`   
text-align: left;
background: #fafafa;
padding:13px;
border-bottom: 1px solid #f0f0f0;
transition: background .3s ease;`;

export const StyledTable = styled.table`  
border-collapse: collapse;
position: relative;
width: 100%;`;


export const StyledTr = styled.tr`  
border-bottom: 1px solid #f0f0f0;
transition: background .3s;
;`;

export const StyledTd = styled.td` 
position: relative;
padding: 16px;
overflow-wrap: break-word;
`;

export const Container = styled.tr` 
position: absolute;
top: 0;
bottom: 0;
left: 0;
right: 0;
margin: auto;
height: fit-content;
width: fit-content;
 padding-top: 200px;
`;