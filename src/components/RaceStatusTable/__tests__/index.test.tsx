import {millisToMinutesAndSeconds,sortRaceResultByTime} from "../../racingResults";
import {RacingResultMap} from "../../types";
import {addWindowMatchMedia, removeWindowMatchMedia} from "../../../utils/testUtils";


const unsortedHorseResult:RacingResultMap = {
    6: {id: 6, name: "Starbolt", time: 12225, event: "finish"},
    16: {id: 16, name: "Domino", time: 0, event: "start"},
    28: {id: 28, name: "Hamlet", time: 11428, event: "finish"}
};
const sortedHorseResult = [
    {id: 28, name: "Hamlet", time: 11428, event: "finish"},
    {id: 6, name: "Starbolt", time: 12225, event: "finish"},
    {id: 16, name: "Domino", time: 0, event: "start"},
]


describe('horse racing results function', () => {
    beforeEach(() => {
       addWindowMatchMedia()
    });
    afterEach(() => {
       removeWindowMatchMedia()
    });
    it('convert milliseconds to minutes and seconds', () => {
        const time = '0:11'
        expect(millisToMinutesAndSeconds(10675)).toBe(time)
    });
    it('should sort the horse racing result by time as ascending order but should put zero at the end', () => {
        expect(sortRaceResultByTime(unsortedHorseResult)).toStrictEqual(sortedHorseResult)
    })
});