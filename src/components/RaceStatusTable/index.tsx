import React, {useContext} from 'react'
import {useHorseData, sortRaceResultByTime, millisToMinutesAndSeconds} from "../racingResults";
import TokenContext from "../../context/globalContextProvider";
import {Spin} from 'antd'
import {StyledTable, StyledTr, StyledTd, StyledTh, Container} from './styled'

const RaceStatusTable = () => {
    const tokenContext = useContext(TokenContext)
    const {currentData = {}, error} = useHorseData(tokenContext.tokenState.token, tokenContext.tokenDispatch);
    const sortedHorseData = sortRaceResultByTime(currentData)

    return (
        <StyledTable data-testid="horseRacingTable">
            <thead>
            <tr>
                <StyledTh>No</StyledTh>
                <StyledTh>Horse Name</StyledTh>
                <StyledTh>Time</StyledTh>
            </tr>
            </thead>
            <tbody>
            {
                !error ? (
                    sortedHorseData.length > 0 ? (
                        sortedHorseData.map((value) => {
                            return (
                                <StyledTr key={`${value.id}_rowId`}>
                                    <StyledTd>{value.id}</StyledTd>
                                    <StyledTd>{value.name}</StyledTd>
                                    <StyledTd>
                                        {value.time > 0 && (
                                            <>
                                                {millisToMinutesAndSeconds(value.time)} s
                                            </>
                                        )}
                                    </StyledTd>
                                </StyledTr>
                            )
                        })
                    ) : (
                        <Container>
                            <td colSpan={3}>
                                <Spin tip="Loading..."/>
                            </td>
                        </Container>
                    )
                ) : (
                    <Container>
                        <td colSpan={3}>
                            <p>Sorry, not able to show the results</p>
                        </td>
                    </Container>
                )
            }
            </tbody>
        </StyledTable>
    )
}

export default RaceStatusTable