import {useEffect, useRef, useState} from "react";
import {RacingResultMap, ActionTypes} from "./types";
import {fetchRacingResults, getLoginCredentials} from "./api";
import {ok, serverIsBusy, unauthorized} from "../utils/systemUtils";
import {userEmail, password} from "../utils/loginConstants";


export const useInterval = (callback: () => void, delay: number | null) => {
    const savedCallback = useRef(callback);

    useEffect(() => {
        savedCallback.current = callback;
    });

    useEffect(
        () => {
            const tick = () => {
                savedCallback.current();
            };

            if (delay !== null) {
                let id = setInterval(tick, delay);
                return () => clearInterval(id);
            }
        },
        [delay]
    );
};

export const millisToMinutesAndSeconds = (millis: number): string => {
    const minutes = Math.floor(millis / 60000);
    const secondsStr = ((millis % 60000) / 1000).toFixed(0);
    const seconds = parseInt(secondsStr);
    return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
}

export const sortRaceResultByTime = (racingResultMap: RacingResultMap) => {
    return Object.values(racingResultMap)
        .sort((a, b) => {
            if (a.time === 0) return 1;
            if (b.time === 0) return -1;
            return a.time - b.time;
        });
}

export const useHorseData = (token: string, setToken: (token: ActionTypes) => void) => {
    const [currentData, setCurrentData] = useState<RacingResultMap>({});
    const [error, setError] = useState<boolean>(false);
    const [interval, setInterval] = useState<number | null>(15000);


    const fetchHorseRacingResults = async () => {
        await fetchRacingResults(token).then((response) => {
            if (response.status === ok) {
                return response.json()
            } else if (response.status === unauthorized) {
                getLoginCredentials({
                    email: userEmail,
                    password: password
                }, setToken)
            } else if (response.status === serverIsBusy) {
                setError(true);
                setInterval(null);
            }
        }).then((data) => {
            if (data) {
                const state = {...currentData}
                state[data.horse.id] = {
                    id: data.horse.id,
                    name: data.horse.name,
                    time: data.event === 'finish' ? data.time : 0,
                };
                setCurrentData(state)
            }
        }).catch((e) => {
            setError(true);
            setInterval(null);
        })
    }
    useInterval(fetchHorseRacingResults, interval)

    return {
        currentData,
        error
    }


}