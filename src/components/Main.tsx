import React, {useContext} from "react";
import TokenContext from "../context/globalContextProvider";
import LoginForm from "./LoginForm";
import RaceStatusTable from "./RaceStatusTable";

function MainComponent() {
    const tokenContext = useContext(TokenContext)

    return (
        <div>
            {tokenContext.tokenState.token.length > 0 ? (
                <RaceStatusTable/>
            ) : (
                <LoginForm/>
            )}
        </div>
    )
}

export default MainComponent