import React from 'react';
import {render} from '@testing-library/react';
import LoginForm from "../index";
import {ContextProvider} from "../../../context/globalContextProvider";
import '@testing-library/jest-dom/extend-expect';
import {addWindowMatchMedia, removeWindowMatchMedia} from "../../../utils/testUtils";

const setup = () => {
    const value = {
        tokenState: {token: ""},
        tokenDispatch: jest.fn()
    };

    const loginForm = render(
        <ContextProvider value={value}>
            <LoginForm/>
        </ContextProvider>,
    );

    return {
        ...loginForm,
    };
};

describe('Login Form Suite', () => {
    beforeEach(() => {
       addWindowMatchMedia()
    });
    afterEach(() => {
     removeWindowMatchMedia()
    });
    it('login form should be in the document', () => {
        const {getByLabelText, getByText} = setup();
        expect(getByLabelText('Email')).toBeInTheDocument()
        expect(getByLabelText('Password')).toBeInTheDocument()
        expect(getByText('Login')).toBeInTheDocument()

    });
})
