import React, {useContext} from 'react'
import {Button, Form, Input, message} from "antd";
import TokenContext from "../../context/globalContextProvider";
import {getLoginCredentials} from '../api'
import {userEmail, password} from "../../utils/loginConstants";
import styled from "styled-components";
import {LoginRequestModel} from "../types";

const Heading = styled.h3`
text-align:center;
padding-bottom:20px;
`;

function LoginForm() {
    const tokenContext = useContext(TokenContext)
    const onFinish = async (values: LoginRequestModel) => {
        getLoginCredentials(values, tokenContext.tokenDispatch)
    };

    const onFinishFailed = (errorInfo: any) => {
        message.error(errorInfo);
    };
    return (
        <>
            <Heading>Login to see horse racing results</Heading>
            <Form
                name="basic"
                labelCol={{span: 8}}
                wrapperCol={{span: 16}}
                initialValues={{remember: true}}
                autoComplete="off"
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                data-testid='loginForm'
            >
                <Form.Item
                    label="Email"
                    name="email"
                    initialValue={userEmail}
                    rules={[
                        {required: true, message: 'Please input your email!'},
                        {type: 'email', message: 'Please enter the valid email'}
                    ]}
                >
                    <Input/>
                </Form.Item>

                <Form.Item
                    label="Password"
                    name="password"
                    rules={[{required: true, message: 'Please input your password!'}]}
                    initialValue={password}
                >
                    <Input.Password/>
                </Form.Item>

                <Form.Item wrapperCol={{offset: 8, span: 16}}>
                    <Button type="primary" htmlType="submit" data-testid='loginButton'>
                        Login
                    </Button>
                </Form.Item>
            </Form>
        </>
    )
}

export default LoginForm