import {message} from "antd";
import {LoginRequestModel, ActionTypes} from "./types";
import {serverUrl} from "../utils/systemUtils";


export const getLoginCredentials = (values: LoginRequestModel, setToken: (token: ActionTypes) => void) => {
    fetch(`${serverUrl}/auth`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(values)
    }).then(response => response.json()).then(res => {
        setToken({
            type: 'ADD_TOKEN',
            payload: res.token
        })
    }).catch(error => message.error(error))
};

export const fetchRacingResults = async (token: string) => {
    return fetch(`${serverUrl}/results`, {
        method: 'GET',
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
};