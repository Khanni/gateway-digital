import React from 'react';
import {render} from '@testing-library/react';
import MainComponent from "./Main";
import {ContextProvider} from "../context/globalContextProvider";
import {addWindowMatchMedia, removeWindowMatchMedia} from "../utils/testUtils";
import '@testing-library/jest-dom/extend-expect';

const setup = (token:string) => {
    const value = {
        tokenState: {token: token},
        tokenDispatch: jest.fn()
    };

    const component = render(
        <ContextProvider value={value}>
            <MainComponent/>
        </ContextProvider>,
    );

    return {
        ...component,
    };
};

describe('Main Component', () => {
    beforeEach(() => {
       addWindowMatchMedia()
    });
    afterEach(() => {
       removeWindowMatchMedia()
    });
    it('Should render loginForm when token is empty', () => {
        const {getByTestId, queryByTestId} = setup('');
        expect(getByTestId('loginForm')).toBeInTheDocument();
        expect(queryByTestId('horseRacingTable')).toBeNull();
    });
    it('Should render RaceStatusTable component when token is not empty', () => {
        const {getByTestId, queryByTestId} = setup('a0ea10099a7564c5aed098fb4c9af83f9c6968dce');
        expect(queryByTestId('loginForm')).toBeNull()
        expect(getByTestId('horseRacingTable')).toBeInTheDocument();
    });
})
