import React from "react";
import {ActionTypes, ContextState, ContextType} from "../components/types";


export const initialState: ContextState = {
    token: ''
}

export const tokenReducer = (state: ContextState, action: ActionTypes) => {
    switch (action.type) {
        case 'ADD_TOKEN':
            return {
                ...state,
                token: action.payload
            }
        default:
            return {
                ...state
            }
    }
}

export const TokenContext = React.createContext<ContextType>({
    tokenState: initialState,
    tokenDispatch: () => {
    }
});

export const ContextConsumer = TokenContext.Consumer
export const ContextProvider = TokenContext.Provider
export default TokenContext
