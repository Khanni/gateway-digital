export const addWindowMatchMedia =() => {
    // add window.matchMedia
    // this is necessary for the date picker to be rendered in desktop mode.
    // if this is not provided, the mobile mode is rendered, which might lead to unexpected behavior
    Object.defineProperty(window, 'matchMedia', {
        writable: true,
        value: (query: string): MediaQueryList => ({
            media: query,
            matches: query === '(pointer: fine)',
            onchange: () => {
            },
            addEventListener: () => {
            },
            removeEventListener: () => {
            },
            addListener: () => {
            },
            removeListener: () => {
            },
            dispatchEvent: () => false,
        }),
    });
}

// @ts-ignore
export const removeWindowMatchMedia = () => delete window.matchMedia;