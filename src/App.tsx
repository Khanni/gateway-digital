import React, {useReducer} from 'react';
import {Row, Col, Layout} from 'antd';
import FooterComponent from "./footer";
import {tokenReducer, initialState, ContextProvider} from "./context/globalContextProvider";
import MainComponent from './components/Main';
import styled from "styled-components";

const {Header, Footer, Content} = Layout;

const StyledHeader = styled.div`
.ant-layout-header{
background:#f0f2f5 !important;
font-weight:bold;
}
`;

const StyledFooter = styled.div`
position: fixed;
bottom: 0;
width: 100%;
text-align:center
`;


function App() {
    const [tokenState, tokenDispatch] = useReducer(tokenReducer, initialState)
    const contextValue = {
        tokenState,
        tokenDispatch
    }
    return (
        <ContextProvider value={contextValue}>
            <Layout>
                <StyledHeader>
                    <Header>
                        Home
                    </Header>
                </StyledHeader>
                <Content style={{backgroundColor: 'white', padding: '120px 80px'}}>
                    <Row>
                        <Col xs={22} lg={{span: 12, offset: 5}}>
                            <MainComponent/>
                        </Col>
                    </Row>
                </Content>
                <StyledFooter>
                    <Footer>
                        <FooterComponent/>
                    </Footer>
                </StyledFooter>
            </Layout>
        </ContextProvider>
    );
}

export default App;
