# Gateway Digital Test


## Run project locally


**Install project dependencies**

### `yarn install`
OR
### `npm install`

**Run the project - In the project directory, you can run:**

### `yarn start`
OR
### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

## Run unit tests and integration tests

**Run unit tests (Jest library is used for unit testing)**
### `yarn test`
OR
### `npm test`

**Run Integration tests (Cypress library is used for integration tests)**
### `yarn cypress`
OR
### `npm cypress`

## UI library used
1. AntDesign UI library is used
2. Styled components used
